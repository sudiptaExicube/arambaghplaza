import { Component, OnInit } from '@angular/core';
import { IonRouterOutlet, ModalController, PopoverController } from '@ionic/angular';

import { KnowMorePage } from '../know-more/know-more.page';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-earning-program',
  templateUrl: './earning-program.page.html',
  styleUrls: ['./earning-program.page.scss'],
})
export class EarningProgramPage implements OnInit {
  earningProgrammeContent:any = "";
  constructor( public global: GlobalService,
    private apiService: ApiService,
    private modalCtrl: ModalController,
    public routerOutlet: IonRouterOutlet,
    public popoverController: PopoverController
    ) { }

  ngOnInit() {
  }

  contactUs(){
    let data = "*Hi Team Arambagh Plaza,*" + "%0a" + "%0a" + "My Name is - " + this.global.userdetails?.name + "%0a" + "---------------------------------------------------------" + "%0a" + "I am Interested to be an agent.";
    console.log(data)
    window.open("https://api.whatsapp.com/send?phone=+917365833384&text=" + data, '_blank');
  }

  ionViewWillEnter() {
    console.log('hi..')
    this.viewEarningProgram(null)
  }

  viewEarningProgram(event) {
    if(event == null){this.global.presentLoadingDefault()}
    this.apiService.getEaningProgContent({})
      .then((success: any) => {
        console.log(success)
        if (success.status == 'success') {
          let successdata: any = success.data;
          this.earningProgrammeContent = success?.data
           console.log(successdata)
           if(event == null){this.global.presentLoadingClose()}else{event.target.complete();}
        } else {
          this.global.presentToast(success.message);
          if(event == null){this.global.presentLoadingClose()}else{event.target.complete();}
        }

      }).catch((err: any) => {
        console.log(err);
        this.global.presentToast(err.message);
        if(event == null){this.global.presentLoadingClose()}else{event.target.complete();}
      })
  }

  // async presentModal() {
  //   const modal = await this.modalController.create({
  //     component: KnowMoreModalPage,
  //     cssClass: 'my-custom-class'
  //   });
  //   return await modal.present();
  // }
 
async presentModal(prize) {
  const popover = await this.popoverController.create({
    component: KnowMorePage,
    cssClass: 'contact-popover',
    translucent: true,
    componentProps: {data:prize}
  });
  await popover.present();

  const { role } = await popover.onDidDismiss();
  console.log('onDidDismiss resolved with role', role);

}
  
  

}
