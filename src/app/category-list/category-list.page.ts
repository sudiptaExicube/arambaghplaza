import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalService } from './../services/global.service';
import { MenuController } from '@ionic/angular';
import { ApiService } from './../services/api.service';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.page.html',
  styleUrls: ['./category-list.page.scss'],
})
export class CategoryListPage implements OnInit {
  public categoryList:any=[];
  cartCount:number;
  public loading = true;
  public defaultImage:any = 'https://miro.medium.com/max/441/1*9EBHIOzhE1XfMYoKz1JcsQ.gif';
  constructor(
    private route: Router,
    public global: GlobalService,
    private menu: MenuController,
    private apiService: ApiService,) {
      this.getAllCategory(null)
    }

  ngOnInit() {
  }
  ionViewWillEnter() {
    // this.getAllCategory(null)
  }

  gotoCart(){
    this.route.navigate(['./product-checkout']);
  }

  doRefresh(event){
    this.getAllCategory(event)
  }

  getAllCategory(event){
    //this.global.presentLoadingDefault()
    this.apiService.homePage('categories/fetch')
    .then((success:any)=>{
      if(success.status == 'success'){
        //this.global.presentLoadingClose()
        console.log(success)
        let masterData = success.data;
        this.categoryList = masterData.categories;
        if(event !=null){ event.target.complete();}
        setTimeout(()=>{
          this.loading=false
        },1000)
      }else{
        //this.global.presentLoadingClose()
        this.global.presentToast(success.message);
        if(event !=null){ event.target.complete();}
        setTimeout(()=>{
          this.loading=false
        },1000)
      }
     
    }).catch((err: any) => {
      console.log(err);
      //this.global.presentLoadingClose()
      this.global.presentToast(err.message);
      if(event !=null){ event.target.complete();}
      setTimeout(()=>{
        this.loading=false
      },1000)
    })
  }

  getCartCount(){
    this.apiService.homePage('cart/count')
    .then((success:any)=>{
      if(success.status == 'success'){
        console.log(success)
        let masterData = success.data;
        this.cartCount = masterData?.cartcount;
      }else{
        this.global.presentToast(success.message);
      }
     
    }).catch((err: any) => {
      console.log(err);
      this.global.presentToast(err.message);
    })
  }

  gotoSubCatPage(item){
    console.log(item)
    this.global.selectedCategory = item.id;
    this.route.navigate(['./subcategory-list']);
    
  }

}
