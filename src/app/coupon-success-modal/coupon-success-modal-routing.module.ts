import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CouponSuccessModalPage } from './coupon-success-modal.page';

const routes: Routes = [
  {
    path: '',
    component: CouponSuccessModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CouponSuccessModalPageRoutingModule {}
