import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-coupon-success-modal',
  templateUrl: './coupon-success-modal.page.html',
  styleUrls: ['./coupon-success-modal.page.scss'],
})
export class CouponSuccessModalPage implements OnInit {
  details:any;
  constructor( public modalCtrl:ModalController,private navParams: NavParams) { }

  ngOnInit() {
    this.details = this.navParams.get('data');
    console.log(this.details);

    setTimeout(()=>{
      this.modalCtrl.dismiss()
    },3000)
   
  }

}
