import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage implements OnInit {
  slideOpts = {
    initialSlide: 0,
    speed: 200,
    autoplay:true
  };
  constructor(private route: Router,) { }

  ngOnInit() {
  }

  gotoSignin(){
    localStorage.setItem('getStartedFlag','true');
    this.route.navigate(['./login-screen']);
    this.play()
  }

  play() {
    var song = new Audio();
    song.src = 'assets/imgs/welcome.mp3';
    song.play();
  }
}


