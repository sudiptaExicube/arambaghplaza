import { Injectable, NgZone } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NavController } from '@ionic/angular';
import { GlobalService } from './global.service';
import { File } from '@ionic-native/file/ngx';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  /* == DEVELOPMENT == */
  // uri = 'https://arambaghplaza.codelution.in/api/';

  /* == LIVE == */
  uri = "https://arambaghplaza.com/api/"

  constructor(
    public http: HttpClient,
    private navCtrl: NavController,
    public zone: NgZone,
    private global: GlobalService,
    private file: File,
  ) { }


  //========================== Login =====================//
  
  apiWithoutToken(paramFields, endPointName) {
    console.log(paramFields);
    return new Promise(resolve => {
      this.http.post(`${this.uri}` + endPointName, paramFields).subscribe(data => {
        //console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }
  getContent(endPointName) {
      return new Promise(resolve => {
        this.http.post(`${this.uri}` + endPointName, {}).subscribe(data => {
          //console.log(data);
          resolve(data);
        }, err => {
          console.log(err);
          resolve(err);
        });
      });
    }
  

  apiWithToken(endPointName) {
    console.log('token', this.global.user_token)
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this.global.user_token,
        "Accept":"application/json"
      })
    };
    return new Promise(resolve => {
      this.http.post(`${this.uri}` + endPointName, {}, httpOptions).subscribe(data => {
        //console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }

  apiWithTokenBody(paramFields,endPointName) {
    console.log('token', this.global.user_token);
    console.log('token', paramFields);
    console.log(`${this.uri}` + endPointName)
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this.global.user_token,
        "Accept":"application/json"
      })
    };
    return new Promise(resolve => {
      this.http.post(`${this.uri}` + endPointName, paramFields, httpOptions).subscribe(data => {
       // console.log(data);
        resolve(data);  
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }

  getApi(endPointName) {
    console.log(this.global.user_token);
    console.log(`${this.uri}` + endPointName)
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this.global.user_token,
        "Accept":"application/json"
      })
    };
    return new Promise(resolve => {
      this.http.get(`${this.uri}` + endPointName, httpOptions).subscribe(data => {
       // console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }

  /* ======================= NEW API START ======================= */

  //Login API
  public signin(params){
    // let fd = new FormData();
    // fd.append("email", params.email);
    // fd.append("password", params.password);

    return new Promise(resolve => {
      this.http.post(`${this.uri}` + 'auth/login', params).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }

  //Signup
  public signup(params){
    // let fd = new FormData();
    // fd.append("email", params.email);
    // fd.append("password", params.password);

    return new Promise(resolve => {
      this.http.post(`${this.uri}` + 'auth/register', params).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }



  /* ========= Product section ================= */

  //Homepage Data load API
  public loadHomepageData(){
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' +this.global.user_token
      })
    };

    return new Promise(resolve => {
      this.http.post(`${this.uri}` + 'homepage/load', {},this.global.user_token ? httpOptions : {}).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }

  public findProductList(params){
    // console.log("this.global.user_token : ", this.global.user_token)
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' +this.global.user_token
      })
    };

    return new Promise(resolve => {
      this.http.post(`${this.uri}` + 'products/browse', params,this.global.user_token ? httpOptions : {}).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }
  // products/browse

  //search product
  searchProduct(params){
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' +this.global.user_token
      })
    };

    return new Promise(resolve => {
      this.http.post(`${this.uri}` + 'products/browse', params,this.global.user_token ? httpOptions : {}).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }

  //Product details
  public loadProductDetails(params){
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' +this.global.user_token
      })
    };

    return new Promise(resolve => {
      this.http.post(`${this.uri}` + 'product/details', params,this.global.user_token ? httpOptions : {}).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }

  //Product variant details
  fetchProductVariantDetails(params){
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' +this.global.user_token
      })
    };

    return new Promise(resolve => {
      this.http.post(`${this.uri}` + 'product/variant-details', params,this.global.user_token ? httpOptions : {}).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }






 /* ========= CART section ================= */

  public fetchCartCount(){
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' +this.global.user_token
      })
    };
    return new Promise(resolve => {
      this.http.post(`${this.uri}` + 'cart/count', {},httpOptions ).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }

  //ADD to Cart
  public addToCart(params){
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' +this.global.user_token
        // 'authorization': this.global.user_token
      })
    };

    return new Promise(resolve => {
      this.http.post(`${this.uri}` + 'cart/add', params,httpOptions ).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }

  // All Cart list
  public fetchCartList(params){
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' +this.global.user_token
      })
    };
    return new Promise(resolve => {
      this.http.post(`${this.uri}` + 'cart/list', params,httpOptions ).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }


  /* ========= ORDER section ================= */

  //Create New order 
  createNewOrder(params){
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' +this.global.user_token
      })
    };
    return new Promise(resolve => {
      this.http.post(`${this.uri}` + 'order/create', params,httpOptions ).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }

  /* ==== Payment callback func ===== */
  paymentcallback(params){
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' +this.global.user_token
      })
    };
    return new Promise(resolve => {
      this.http.post(`${this.uri}` + 'order/payment-callback', params,httpOptions ).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }


  /* == Generate Order ID for Cash orders ==== */
  generateOrderIDforCash(params){
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' +this.global.user_token
      })
    };
    return new Promise(resolve => {
      this.http.post(`${this.uri}` + 'order/cash-order/initiate-payment', params,httpOptions ).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }

  /* ==== Payment callback func for CASH ===== */
  paymentcallback_forCash(params){
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' +this.global.user_token
      })
    };
    return new Promise(resolve => {
      this.http.post(`${this.uri}` + 'order/cash-order/payment-callback', params,httpOptions ).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }

  /* ========= Address management ================= */
  setDefaultAddress(params){
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' +this.global.user_token
      })
    };

    return new Promise(resolve => {
      this.http.post(`${this.uri}` + 'user/address/set-default', params,this.global.user_token ? httpOptions : {}).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }

  deleteSavedAddress(params){
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' +this.global.user_token
      })
    };

    return new Promise(resolve => {
      this.http.post(`${this.uri}` + 'user/address/delete', params,this.global.user_token ? httpOptions : {}).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }


 
  
  Otpmanagement(params,endpoint){
    console.log(`${this.uri}` + 'otp/user-login/'+endpoint)
    return new Promise(resolve => {
      this.http.post(`${this.uri}`+endpoint, params).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }

  homePage(endpoint){
    console.log(`${this.uri}` +endpoint)
    return new Promise(resolve => {
      this.http.post(`${this.uri}`+endpoint, {}).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }

  // NOTIFICATION
  fetchNotifications(){
    
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' +this.global.user_token
      })
    };

    return new Promise(resolve => {
      this.http.post(`${this.uri}` + 'user/notifications', {},this.global.user_token ? httpOptions : {}).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }

  //ADD CUSTOMER

  public addDownlineCustomer(params){
    // let fd = new FormData();
    // fd.append("email", params.email);
    // fd.append("password", params.password);
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' +this.global.user_token
      })
    };
    return new Promise(resolve => {
      this.http.post(`${this.uri}` + 'member/add', params,this.global.user_token ? httpOptions : {}).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }

  public checkWalletBal(params){
    // let fd = new FormData();
    // fd.append("email", params.email);
    // fd.append("password", params.password);
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' +this.global.user_token
      })
    };
    return new Promise(resolve => {
      this.http.post(`${this.uri}` + 'wallet/balance', params,this.global.user_token ? httpOptions : {}).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }

  public payoutRequest(params){
     let fd = new FormData();
     fd.append("amount", params.amount);
     fd.append("remarks", params.remarks);
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' +this.global.user_token
      })
    };
    return new Promise(resolve => {
      this.http.post(`${this.uri}` + 'wallet/payout/request/submit', params,this.global.user_token ? httpOptions : {}).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }

  getDownlineMembers(params){
    // let fd = new FormData();
    // fd.append("email", params.email);
    // fd.append("password", params.password);
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' +this.global.user_token
      })
    };
    return new Promise(resolve => {
      this.http.post(`${this.uri}` + 'member/fetch', params,this.global.user_token ? httpOptions : {}).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }


getEaningProgContent(params){
  let httpOptions = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' +this.global.user_token
    })
  };
  return new Promise(resolve => {
    this.http.post(`${this.uri}` + 'schemes/fetch', {}).subscribe(data => {
      resolve(data);
    }, err => {
      console.log(err);
      resolve(err);
    });
  });
}

transactionHistory(endpoint){
  console.log(this.uri + endpoint)
  let httpOptions = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' +this.global.user_token
    })
  };
  return new Promise(resolve => {
    this.http.post(`${this.uri}` + endpoint, {}, httpOptions).subscribe(data => {
      resolve(data);
    }, err => {
      console.log(err);
      resolve(err);
    });
  });
}

}





// this.apiService.getApi('auth/account')
//     .then((success:any)=>{
//       if(success.status == 'success'){
//         console.log(success)
//         this.global.presentToast(success.message);
//         this.global.presentLoadingClose()
//       }else{
//         this.global.presentToast(success.message);
//         this.global.presentLoadingClose()
//       }
     
//     }).catch((err: any) => {
//       console.log(err);
//       this.global.presentToast(err.message);
//       this.global.presentLoadingClose()
//     })