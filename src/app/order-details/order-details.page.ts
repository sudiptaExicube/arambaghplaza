import { Component, OnInit } from '@angular/core';
import { ActionSheetController, ModalController, AlertController, NavController } from '@ionic/angular';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { GlobalService } from './../services/global.service';
import { ApiService } from './../services/api.service';
import { RatingPagePage } from './../rating-page/rating-page.page';
import { CallNumber } from '@ionic-native/call-number/ngx';

declare var RazorpayCheckout: any;

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.page.html',
  styleUrls: ['./order-details.page.scss'],
})
export class OrderDetailsPage implements OnInit {
  statusArr = [
    { name: "Ordered", date: "Mon,13th Sep '2021 " },
    { name: "Shipped", date: "" },
    { name: "Out For Delivery", date: "" },
    { name: "Delivery", date: "" }
  ]

  public showTrack: boolean = false;
  public order_id: any;
  loading: boolean = true;
  orderDetails: any;
  constructor(
    public actionSheetController: ActionSheetController,
    private route: Router,
    public activeRoute: ActivatedRoute,
    public global: GlobalService,
    public apiService: ApiService,
    public modalController: ModalController,
    public alertController: AlertController,
    private callNumber: CallNumber,
    public navCtrl: NavController,
  ) {
    // this.navCtrl.navigateRoot(['./payment-success'])
  }

  ngOnInit() {
    this.activeRoute.queryParams.subscribe(params => {
      console.log("params : ", params)
      this.order_id = params["order_id"];
      if (this.order_id) {
        this.fetchOrderDetails(null);
      }
    })
  }

  dateCnvrt(dateP) {
    var date = new Date(dateP);
    var dateString = new Date(date.getTime() - (date.getTimezoneOffset() * 60000))
      .toISOString()
      .split("T")[0];
    return dateString
  }
  doRefresh(event) {
    this.loading = true;
    this.fetchOrderDetails(event)
  }
  fetchOrderDetails(event) {
    this.apiService.apiWithTokenBody({ order_id: this.order_id }, 'order/details')
      .then((success: any) => {
        if (success.status == 'success') {
          console.log(success)
          let masterData = success.data;
          this.orderDetails = masterData?.order;
          this.orderDetails.delivery_charge = parseFloat(this.orderDetails.delivery_charge)
          console.log(this.orderDetails);
          if (event != null) { event.target.complete(); }
          setTimeout(() => {
            this.loading = false
          }, 1000)
        } else {
          this.global.presentToast(success.message);
          if (event != null) { event.target.complete(); }
          setTimeout(() => {
            this.loading = false
          }, 1000)
        }

      }).catch((err: any) => {
        if (event != null) { event.target.complete(); }
        console.log(err);
        this.global.presentToast(err.message);
        setTimeout(() => {
          this.loading = false
        }, 1000)
      })
  }
  public showTracking() {
    this.showTrack = !this.showTrack
  }

  callNow(mobile) {
    let date = new Date();
    let n = date.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: false });
    let time = n.split(':')[0];

    if (parseInt(time) <= 22 && (parseInt(time) >= 9)) {
      this.callNumber.callNumber(mobile, true)
        .then(res => console.log('Launched dialer!', res))
        .catch(err => console.log('Error launching dialer', err));
    } else {
      this.global.presentToast("Sorry !!! We are unable to take your call at this moment. Please call us between 9:00 AM - 10:00 PM");
    }
  }

  cancelOrderConfirmation() {
    this.alertController.create({
      header: 'Confirm Alert',
      message: 'Are you sure? you want to cancel order?',
      buttons: [

        {
          text: 'No',
          handler: () => {
            console.log('Let me think');
          }
        },
        {
          text: 'Yes!',
          handler: () => {
            if (this.orderDetails?.payment_mode == 'online') {
               if (this.orderDetails?.status == 'received' || this.orderDetails?.status == 'intransit' || this.orderDetails?.status == 'accepted') {
                let navigationExtras: NavigationExtras = {
                  queryParams: {
                    order_id: this.orderDetails?.code,
                  }
                }
                this.route.navigate(['./support'], navigationExtras);
              } else {
                this.global.presentToast("Sorry! We are unable to cancel your order for this scenario.Please call to store for better help")
              }
            } else {
              if (this.orderDetails?.status == 'received') {
                this.global.presentLoadingDefault();
                this.apiService.apiWithTokenBody({ order_id: this.order_id }, 'order/cancel-submit')
                  .then((success: any) => {
                    if (success.status == 'success') {
                      console.log(success)
                      this.global.presentLoadingClose()
                      this.global.presentToast(success.message);
                      this.fetchOrderDetails(null);
                    } else {
                      this.global.presentLoadingClose()
                      this.global.presentToast(success.message);
                      this.fetchOrderDetails(null);
                    }

                  }).catch((err: any) => {
                    this.global.presentLoadingClose()
                    this.global.presentToast(err.message);
                  })
              } else if (this.orderDetails?.status == 'cancelled') {
                this.global.presentToast("This order is already cancelled")
              } else if (this.orderDetails?.status == 'intransit' || this.orderDetails?.status == 'accepted') {
                let navigationExtras: NavigationExtras = {
                  queryParams: {
                    order_id: this.orderDetails?.code,
                  }
                }
                this.route.navigate(['./support'], navigationExtras);
              } else {
                this.global.presentToast("Sorry! We are unable to cancel your order for this scenario.Please call to store for better help")
              }
            }
          }
        }
      ]
    }).then(res => {
      res.present();
    });
  }

  replaceOrderConfirmation() {
    this.alertController.create({
      header: 'Confirm Alert',
      message: 'Are you sure? you want to replace order?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Let me think');
          }
        },
        {
          text: 'Yes!',
          handler: () => {
            this.order_id = this.order_id;
            let navigationExtras: NavigationExtras = {
              queryParams: {
                order_id: this.order_id,
              }
            }
            this.route.navigate(['./support'], navigationExtras);
          }
        }
      ]
    }).then(res => {
      res.present();
    });
  }

  support() {
    this.alertController.create({
      header: 'Confirm Alert',
      message: 'Have any query or problem regarding this order?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Let me think');
          }
        },
        {
          text: 'Yes!',
          handler: () => {
            this.order_id = this.order_id;
            let navigationExtras: NavigationExtras = {
              queryParams: {
                order_id: this.order_id,
              }
            }
            this.route.navigate(['./support'], navigationExtras);
          }
        }
      ]
    }).then(res => {
      res.present();
    });
  }

  async rating() {
    const modal = await this.modalController.create({
      component: RatingPagePage,
      cssClass: 'my-custom-class',
      componentProps: { order_id: this.order_id }
    });
    await modal.present();
    modal.onDidDismiss()
      .then((data: any) => {
        console.log(data);
        if (data.data != null) {
          this.order_id = data.data?.order_id;
          console.log(this.order_id)
          this.fetchOrderDetails(null);
        }
      });
  }

  download(url) {
    window.open(url);
  }


  /* == Pay now === */
  paynow() {
    this.global.presentLoadingDefault();
    let paramData = {
      order_id: this.orderDetails.id
    }
    this.apiService.generateOrderIDforCash(paramData)
      .then((success: any) => {
        console.log("paymentCallbackFunc success : ", success)
        if (success.status == 'success') {
          this.global.presentLoadingClose();
          console.log(success);
          this.startRazorpay(success.data.rzpy_document, success.data)

        } else {
          this.global.presentLoadingClose();
          this.global.presentToast(success.message);
        }

      })
      .catch((error: any) => {
        this.global.presentLoadingClose()
        console.log("Order now error : ", error)
      })
  }

  /* == Razorpay payment start ==== */
  startRazorpay(details, orderData) {
    // var options = {
    //   description: 'Pay online',
    //   image: 'https://i.imgur.com/3g7nmJC.png',
    //   currency: 'INR',
    //   key:details?.key_id,
    //   amount:details?.amount,
    //   name: this.global.userdetails?.name,
    //   theme: {
    //     color: '#3399cc'
    //   }
    // }

    var options = {
      description: 'Pay online',
      image: 'https://arambaghplaza.com/public/favicon.png',
      currency: 'INR',
      // key:'rzp_live_K2PU2QUvfNlYpj',
      // key:'rzp_test_Wy1EiZs2Miv4bd',
      key: details?.key_id,
      amount: details?.amount,
      name: "Arambagh Plaza Retail Store",
      prefill: {
        email: this.global.userdetails?.email,
        contact: this.global.userdetails.mobile,
        name: this.global.userdetails?.name
      },
      theme: {
        color: '#F37254'
      },
      modal: {
        ondismiss: function () {
          // alert('dismissed')
        }
      }
    };
    var successCallback = function (success) {
      console.log("payment success : ", success)
      let orderId = success.razorpay_order_id
      let paymentId = success.razorpay_payment_id;
      let signature = success.razorpay_signature
      if (success.razorpay_payment_id) {
        this.paymentCallbackFunc(orderData, success.razorpay_payment_id)
      }


    }.bind(this)
    var cancelCallback = function (error) {
      console.log("payment error : ", error)
      // alert('payment failed: ' + JSON.stringify(error))
      // alert(error.description);
      if(error.description){
        alert(error.description);
      }else if(error.error){
        if(error.error.description){
          alert(error.error.description);
        }
      }else{
        alert("Payment cancelled, please try again");
      }

    }.bind(this)
    RazorpayCheckout.on('payment.success', successCallback)
    RazorpayCheckout.on('payment.cancel', cancelCallback)
    RazorpayCheckout.open(options)
  }


  /* ===== Payment callback func ==== */
  paymentCallbackFunc(oDetails, razorpay_payment_id) {
    // alert("hello")
    this.global.presentLoadingDefault();
    let paramData = {
      order_id: oDetails.order.id,
      razorpay_payment_id: razorpay_payment_id
    }
    this.apiService.paymentcallback_forCash(paramData)
      .then((success: any) => {
        console.log("paymentCallbackFunc success : ", success)
       // alert(JSON.stringify(success))
        if (success.status == 'success') {
          this.global.presentLoadingClose();
          let navigationExtras: NavigationExtras = {
            queryParams: {
              order_code: success?.data?.order?.payment_txnid,
              messge: success?.message
            }
          }
          this.route.navigate(['./payment-success'], navigationExtras)
        } else {
          this.global.presentLoadingClose();
          this.global.presentToast(success.message);
        }

      })
      .catch((error: any) => {
        this.global.presentLoadingClose()
        console.log("Order now error : ", error)
      })
  }




  /* === */
  // this.global.presentLoadingDefault();
  //   let paramData = {
  //     order_id:oDetails.order.id,
  //     razorpay_payment_id:razorpay_payment_id
  //   }
  //   alert(JSON.stringify(paramData));
  //   this.apiService.paymentcallback(paramData)
  //     .then((success: any) => {
  //       console.log("paymentCallbackFunc success : ", success)
  //       if (success.status == 'success') {
  //         this.global.presentLoadingClose();
  //         alert(JSON.stringify(success))
  //         this.global.globalCartCount = 0;
  //         this.global.globalCartPrice = "";
  //         let navigationExtras: NavigationExtras = {
  //           queryParams: {
  //             order_code: success?.data?.order?.code
  //           }
  //         }
  //         this.navCtrl.navigateRoot(['./successfull-screen'], navigationExtras)
  //       } else {
  //         this.global.presentLoadingClose();
  //         this.global.presentToast(success.message);
  //       }

  //     })
  //     .catch((error: any) => {
  //       this.global.presentLoadingClose()
  //       console.log("Order now error : ", error)
  //     })

}
