import { Component, OnInit } from '@angular/core';
import { CalendarComponentOptions } from 'ion2-calendar';
import { ActionSheetController, ModalController } from '@ionic/angular';
import * as moment from 'moment';
import { GlobalService } from '../services/global.service';
@Component({
  selector: 'app-calender-modal',
  templateUrl: './calender-modal.page.html',
  styleUrls: ['./calender-modal.page.scss'],
})
export class CalenderModalPage implements OnInit {

  date: any;
  type: 'string';
  _daysConfig: any = [];

  optionsMulti: CalendarComponentOptions = {
    pickMode: 'range',
    color: 'danger',
    showMonthPicker: true,
    from: new Date(1),

    //   daysConfig : [{
    //     date: new Date(2020, 3, 24),
    //     disable: false
    // }]
  };

  constructor(public modalController: ModalController,public global: GlobalService,) {


  }

  ngOnInit() {
  }

  dismissModal() {
    console.log(this.date)
    if (this.date != undefined) {
      console.log(moment(this.date?.from._i).format("YYYY-MM-DD"))
      console.log(moment(this.date?.to._i).format("YYYY-MM-DD"))
      this.modalController.dismiss({
        from: moment(this.date?.from._i).format("YYYY-MM-DD"),
        to: moment(this.date?.to._i).format("YYYY-MM-DD")
      })
    }else{
      this.global.presentToast('Please select date range');
    }


  }

  onChange(ev) {
    console.log(ev)
  }
}
