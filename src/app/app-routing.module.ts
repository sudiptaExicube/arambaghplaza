import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'intro',
    pathMatch: 'full'
  },
 
  {
    path: 'intro',
    loadChildren: () => import('./intro/intro.module').then( m => m.IntroPageModule)
  },{
    path: 'login-screen',
    loadChildren: () => import('./login-screen/login-screen.module').then( m => m.LoginScreenPageModule)
  },
  {
    path: 'signup-screen',
    loadChildren: () => import('./signup-screen/signup-screen.module').then( m => m.SignupScreenPageModule)
  },
  {
    path: 'otp-screen',
    loadChildren: () => import('./otp-screen/otp-screen.module').then( m => m.OtpScreenPageModule)
  },
  
  {
    path: 'successfull-screen',
    loadChildren: () => import('./successfull-screen/successfull-screen.module').then( m => m.SuccessfullScreenPageModule)
  },
  
  {
    path: 'calender-modal',
    loadChildren: () => import('./calender-modal/calender-modal.module').then( m => m.CalenderModalPageModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule)
  },
  {
    path: 'address-modal',
    loadChildren: () => import('./address-modal/address-modal.module').then( m => m.AddressModalPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'change-password',
    loadChildren: () => import('./change-password/change-password.module').then( m => m.ChangePasswordPageModule)
  },
  
  {
    path: 'product-list',
    loadChildren: () => import('./product-list/product-list.module').then( m => m.ProductListPageModule)
  },
  {
    path: 'main-dashboard',
    loadChildren: () => import('./main-dashboard/main-dashboard.module').then( m => m.MainDashboardPageModule)
  },
  {
    path: 'category-list',
    loadChildren: () => import('./category-list/category-list.module').then( m => m.CategoryListPageModule)
  },
  {
    path: 'edit-account',
    loadChildren: () => import('./edit-account/edit-account.module').then( m => m.EditAccountPageModule)
  },
  {
    path: 'address-list',
    loadChildren: () => import('./address-list/address-list.module').then( m => m.AddressListPageModule)
  },
  {
    path: 'add-address',
    loadChildren: () => import('./add-address/add-address.module').then( m => m.AddAddressPageModule)
  },
  {
    path: 'about-us',
    loadChildren: () => import('./about-us/about-us.module').then( m => m.AboutUsPageModule)
  },
  {
    path: 'privacy-policy',
    loadChildren: () => import('./privacy-policy/privacy-policy.module').then( m => m.PrivacyPolicyPageModule)
  },
  {
    path: 'support',
    loadChildren: () => import('./support/support.module').then( m => m.SupportPageModule)
  },
 
  {
    path: 'terms-page',
    loadChildren: () => import('./terms-page/terms-page.module').then( m => m.TermsPagePageModule)
  },
  {
    path: 'replacement-policy',
    loadChildren: () => import('./replacement-policy/replacement-policy.module').then( m => m.ReplacementPolicyPageModule)
  },
  {
    path: 'rating-page',
    loadChildren: () => import('./rating-page/rating-page.module').then( m => m.RatingPagePageModule)
  },
  {
    path: 'category-list',
    loadChildren: () => import('./category-list/category-list.module').then( m => m.CategoryListPageModule)
  },
  {
    path: 'product-details',
    loadChildren: () => import('./product-details/product-details.module').then( m => m.ProductDetailsPageModule)
  },
  {
    path: 'product-checkout',
    loadChildren: () => import('./product-checkout/product-checkout.module').then( m => m.ProductCheckoutPageModule)
  },
  {
    path: 'my-orders',
    loadChildren: () => import('./my-orders/my-orders.module').then( m => m.MyOrdersPageModule)
  },
  {
    path: 'order-details',
    loadChildren: () => import('./order-details/order-details.module').then( m => m.OrderDetailsPageModule)
  },
  {
    path: 'promo-codes',
    loadChildren: () => import('./promo-codes/promo-codes.module').then( m => m.PromoCodesPageModule)
  },
  {
    path: 'subcategory-list',
    loadChildren: () => import('./subcategory-list/subcategory-list.module').then( m => m.SubcategoryListPageModule)
  },
  {

    path: 'notifications',
    loadChildren: () => import('./notifications/notifications.module').then( m => m.NotificationsPageModule)
  },
  {
    path: 'product-search',
    loadChildren: () => import('./product-search/product-search.module').then( m => m.ProductSearchPageModule)

  },
  {
    path: 'coupon-success-modal',
    loadChildren: () => import('./coupon-success-modal/coupon-success-modal.module').then( m => m.CouponSuccessModalPageModule)
  },
  {
    path: 'wishlist',
    loadChildren: () => import('./wishlist/wishlist.module').then( m => m.WishlistPageModule)
  },
  {
    path: 'payment-success',
    loadChildren: () => import('./payment-success/payment-success.module').then( m => m.PaymentSuccessPageModule)
  },
  {
    path: 'support-list',
    loadChildren: () => import('./support-list/support-list.module').then( m => m.SupportListPageModule)
  },
  {
    path: 'add-customer',
    loadChildren: () => import('./add-customer/add-customer.module').then( m => m.AddCustomerPageModule)
  },
  {
    path: 'earning-program',
    loadChildren: () => import('./earning-program/earning-program.module').then( m => m.EarningProgramPageModule)
  },
  {
    path: 'wallet',
    loadChildren: () => import('./wallet/wallet.module').then( m => m.WalletPageModule)
  },
  {
    path: 'downline-members',
    loadChildren: () => import('./downline-members/downline-members.module').then( m => m.DownlineMembersPageModule)
  },
  
  {
    path: 'know-more',
    loadChildren: () => import('./know-more/know-more.module').then( m => m.KnowMorePageModule)
  },  {
    path: 'transaction-history',
    loadChildren: () => import('./transaction-history/transaction-history.module').then( m => m.TransactionHistoryPageModule)
  },
  {
    path: 'payout-request',
    loadChildren: () => import('./payout-request/payout-request.module').then( m => m.PayoutRequestPageModule)
  },
  {
    path: 'customer-order-details-propover',
    loadChildren: () => import('./customer-order-details-propover/customer-order-details-propover.module').then( m => m.CustomerOrderDetailsPropoverPageModule)
  }




];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
