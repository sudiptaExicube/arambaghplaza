import { Component, Input, OnInit } from '@angular/core';
import { ModalController, Platform } from '@ionic/angular';

@Component({
  selector: 'app-promo-codes',
  templateUrl: './promo-codes.page.html',
  styleUrls: ['./promo-codes.page.scss'],
})
export class PromoCodesPage implements OnInit {
  subscription: any;
  public couponLists: any = [];
  public coupon_codeName: any = "";
  @Input() coupon_codes: any;

  constructor(public modalController: ModalController, private platform: Platform) {
    // this.platform.backButton.subscribeWithPriority(100, () => {
    //   this.modalController.dismiss(null)
    // });
    this.subscription = this.platform.backButton.subscribeWithPriority(100, () => {
      this.modalController.dismiss(null)
    })
  }

  ngOnInit() {
    console.log("data: ", this.coupon_codes);
    for (let i = 0; i < this.coupon_codes.length; i++) {
      this.coupon_codes[i].flag = false
    }
  }


  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


  dismiss() {
    this.modalController.dismiss(null)
  }

  selectCode(item) {

    this.modalController.dismiss(JSON.stringify(item))
  }

  applyCoupon() {
    if (this.coupon_codeName != "") {
      // let data = 
      this.modalController.dismiss(JSON.stringify(this.coupon_codeName))
    }
  }

  showHide(index) {
    for (let i = 0; i < this.coupon_codes.length; i++) {
      if (i == index) {
        this.coupon_codes[index].flag = this.coupon_codes[index].flag == true ? false : true;
      }
    }
  }
}
