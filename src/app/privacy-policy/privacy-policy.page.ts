import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../services/global.service';
import { ApiService } from './../services/api.service';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.page.html',
  styleUrls: ['./privacy-policy.page.scss'],
})
export class PrivacyPolicyPage implements OnInit {
  public content:any = '';
  loading:boolean = true;
  constructor(public global: GlobalService,
    private apiService: ApiService,) { }

  ngOnInit() {
    this.getData()
  }

  getData() {
    this.apiService.apiWithoutToken({slug:'privacy-policy'},'cms/contents')
      .then((success: any) => {
        if (success.status == 'success') {
          console.log(success)
           this.content = success?.data?.cmscontent?.content;
           console.log(this.content)
          setTimeout(() => {
            this.loading = false
          }, 1000)
        } else {
          this.global.presentToast(success.message);
          setTimeout(() => {
            this.loading = false
          }, 1000)
        }

      }).catch((err: any) => {
        console.log(err);
        this.global.presentToast(err.message);
        setTimeout(() => {
          this.loading = false
        }, 1000)
      })
  }

}
