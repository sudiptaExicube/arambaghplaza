import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CalenderModalPageModule } from './calender-modal/calender-modal.module';

import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';

// import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { File } from '@ionic-native/file/ngx';
// import { Base64 } from '@ionic-native/base64/ngx';
import { DomSanitizer } from '@angular/platform-browser';
import { FilePath } from '@ionic-native/file-path/ngx';

import { PhotoViewer } from '@awesome-cordova-plugins/photo-viewer/ngx';
import { OneSignal } from '@ionic-native/onesignal/ngx';

import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { AppVersion } from '@awesome-cordova-plugins/app-version/ngx';
// import { SmsRetriever } from '@awesome-cordova-plugins/sms-retriever';

//Import the Social Sharing plugin 
// import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    
    CalenderModalPageModule,
    HttpClientModule,
  ],
  providers: [
    Geolocation,
    NativeGeocoder,
    AndroidPermissions,
    LocationAccuracy,
    // Camera,
    File,
    FilePath,
    // Base64,
    PhotoViewer,
    OneSignal,
    Diagnostic,
    CallNumber,
    AppVersion,
    // SmsRetriever,
    SocialSharing,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
