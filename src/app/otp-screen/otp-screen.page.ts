import { Component, OnInit } from '@angular/core';

import { GlobalService } from '../services/global.service';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { ApiService } from './../services/api.service';

// import { SmsRetriever } from '@awesome-cordova-plugins/sms-retriever';


@Component({
  selector: 'app-otp-screen',
  templateUrl: './otp-screen.page.html',
  styleUrls: ['./otp-screen.page.scss'],
})
export class OtpScreenPage implements OnInit {
  public flag: boolean = true;
  public otp: any = {
    input1: "", input2: "", input3: "", input4: ""
  };
  public field1 = false
  public field2 = false
  public field3 = false
  public field4 = false
  public verification_token: any;
  source: any;
  constructor(
    public global: GlobalService,
    private route: Router,
    public activeRoute: ActivatedRoute,
    public apiService: ApiService,
    // private smsRetriever: SmsRetriever
  ) {

   }

  ngOnInit() {
    this.activeRoute.queryParams.subscribe(params => {
      this.verification_token = params.otp_token;
      this.source = params.from
      console.log(this.verification_token)
    })    
  }

  // ionViewWillEnter(){
  //   this.smsRetriever.getAppHash()
  //   .then((res: any) => console.log("getAppHash :", res))
  //   .catch((error: any) => console.error(error));

  //   this.smsRetriever.startWatching()
  //   .then((res: any) => console.log("startWatching success:", res))
  //   .catch((error: any) => console.error(error));
  // }

  inputFocus(field) {
    if (field == '1') {
      this.field1 = true
      this.field2 = false
      this.field3 = false
      this.field4 = false
    } else if (field == '2') {
      this.field1 = false
      this.field2 = true
      this.field3 = false
      this.field4 = false
    } else if (field == '3') {
      this.field1 = false
      this.field2 = false
      this.field3 = true
      this.field4 = false
    } else if (field == '4') {
      this.field1 = false
      this.field2 = false
      this.field3 = false
      this.field4 = true
    }

  }


  moveFocus(next: any, event: any, prev: any, field) {
    console.log(next);
    console.log(event)
    console.log(prev)
    console.log(field)

    if (event.key == 'Backspace') {
      prev.setFocus();
      if (field == '3') {
        this.field1 = false
        this.field2 = true
        this.field3 = false
        this.field4 = false
      } else if (field == '2') {
        this.field1 = true
        this.field2 = false
        this.field3 = false
        this.field4 = false
      } else if (field == '4') {
        this.field1 = false
        this.field2 = false
        this.field3 = true
        this.field4 = false
      }
    } else {
      next.setFocus();
      if (field == '1') {
        this.field1 = false
        this.field2 = true
        this.field3 = false
        this.field4 = false
      } else if (field == '2') {
        this.field1 = false
        this.field2 = false
        this.field3 = true
        this.field4 = false
      } else if (field == '3') {
        this.field1 = false
        this.field2 = false
        this.field3 = false
        this.field4 = true

      } else {
        setTimeout(() => {
          console.log(this.otp.input1 + this.otp.input2 + this.otp.input3 + this.otp.input4);
          this.global.presentLoadingDefault()
          let data = {
            verification_token: this.verification_token,
            otp: this.otp.input1 + this.otp.input2 + this.otp.input3 + this.otp.input4
          }
          console.log(data);
          if (data) {
            let endpoint = this.source == "login"?"otp/user-login/verify":"otp/user-register/verify";
            this.apiService.Otpmanagement(data,endpoint)
            .then((success:any)=>{
              console.log(success);
              if(success?.status == 'error'){
                this.global.presentToast(success.message);
                this.global.presentLoadingClose()
              }else{
                this.global.presentLoadingClose()
                this.global.presentToast(success.message);
                let successdata:any = success.data; 
                localStorage.setItem("access_token",successdata.token.access_token)
                localStorage.setItem("user_details",JSON.stringify(successdata.user))
                this.global.userdetails = successdata.user;
                this.global.selectedScheme = successdata?.scheme;
                this.global.user_token = successdata.token.access_token;
                this.route.navigate(['./main-dashboard']);
              }
              
            }).catch((error:any)=>{
              this.global.presentLoadingClose()
              this.global.presentToast(error.message);
              console.log("OTP verification error : ", error)
            })
          }
        }, 200);
        
        
      }
    }
  }

  resendOTP() {
    this.global.presentLoadingDefault();
    let data = {
      verification_token: this.verification_token,
    }
    let endpoint = this.source == "login" ? "otp/user-login/resend" : "otp/user-register/resend";
    console.log(this.verification_token)
    this.apiService.Otpmanagement(data, endpoint)
      .then((success: any) => {
        console.log(success);
        this.global.presentLoadingClose()
        // this.global.presentToast(success.message);
        this.global.presentToast("OTP sent to your registered email, please varify");
      }).catch((error: any) => {
        this.global.presentLoadingClose()
        this.global.presentToast(error.message);
      })
  }





//   otpController(event,next,prev){
//     if(event.target.value.length < 1 && prev){
//       prev.setFocus()
//     }
//     else if(next && event.target.value.length>0){
//       next.setFocus();
//     }
//     else {
      
      
//       console.log("all field submitted : ", this.otp.input1 + this.otp.input2 + this.otp.input3 + this.otp.input4)

//       setTimeout(() => {
//         console.log(this.otp);
//       }, 200);
//     //  return 0;
//     } 
//  }
}
