import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PayoutRequestPage } from './payout-request.page';

const routes: Routes = [
  {
    path: '',
    component: PayoutRequestPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PayoutRequestPageRoutingModule {}
