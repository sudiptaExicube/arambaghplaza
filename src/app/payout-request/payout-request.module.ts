import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PayoutRequestPageRoutingModule } from './payout-request-routing.module';

import { PayoutRequestPage } from './payout-request.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PayoutRequestPageRoutingModule
  ],
  declarations: [PayoutRequestPage]
})
export class PayoutRequestPageModule {}
