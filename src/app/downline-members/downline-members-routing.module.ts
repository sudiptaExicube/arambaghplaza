import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DownlineMembersPage } from './downline-members.page';

const routes: Routes = [
  {
    path: '',
    component: DownlineMembersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DownlineMembersPageRoutingModule {}
