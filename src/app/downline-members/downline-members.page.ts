import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { AlertController, MenuController, NavController, Platform } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-downline-members',
  templateUrl: './downline-members.page.html',
  styleUrls: ['./downline-members.page.scss'],
})
export class DownlineMembersPage implements OnInit {
 customers:any;
  constructor(   platform: Platform,
    private route: Router,
    public global: GlobalService,
    private menu: MenuController,
    private apiService: ApiService,
    public navCtrl: NavController,
    public alertController: AlertController,
    private callNumber: CallNumber,) { }

  ngOnInit() {
  }

  addNewCustomer(){
    this.route.navigate(['./add-customer']);
  }

  ionViewWillEnter() {
    this.viewDownlineMembers(null)
  }

  viewDownlineMembers(event) {
    if(event == null){this.global.presentLoadingDefault()}
    this.apiService.getDownlineMembers({})
      .then((success: any) => {
        console.log(success)
        if (success.status == 'success') {
          let successdata: any = success.data;
          this.customers = successdata?.users;
           //console.log(successdata)
           if(event == null){this.global.presentLoadingClose()}else{event.target.complete();}
        } else {
          this.global.presentToast(success.message);
          if(event == null){this.global.presentLoadingClose()}else{event.target.complete();}
        }

      }).catch((err: any) => {
        console.log(err);
        this.global.presentToast(err.message);
        if(event == null){this.global.presentLoadingClose()}else{event.target.complete();}
      })
  }

  doRefresh(event){
    this.viewDownlineMembers(event)
  }


    callNow(mobile) {
        this.callNumber.callNumber(mobile, true)
          .then(res => console.log('Launched dialer!', res))
          .catch(err => console.log('Error launching dialer', err));
    }
  
  
}
