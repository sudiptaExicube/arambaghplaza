import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ApiService } from './../services/api.service';
import { IonContent, MenuController } from '@ionic/angular';
import { GlobalService } from './../services/global.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.page.html',
  styleUrls: ['./product-list.page.scss'],
})
export class ProductListPage implements OnInit {
  public subcategoryList: any = []

  public segment: any;
  public showCount: boolean = false;
  public cartCount: number;

  public productList: any
  public loading: boolean = true;

  public subCat_id: any = "";
  public defaultImage: any = 'https://miro.medium.com/max/441/1*9EBHIOzhE1XfMYoKz1JcsQ.gif';

  @ViewChild('scroll') scroll: any;


  constructor(
    private route: Router,
    public global: GlobalService,
    private menu: MenuController,
    private apiService: ApiService,
    public activeRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.activeRoute.queryParams.subscribe(params => {
      console.log("params : ", params)
      this.subCat_id = params["subCat_id"];
      if (this.subCat_id) {
        this.segment = this.subCat_id;
        console.log(this.segment)

      }
    })

  }

  ionViewWillEnter() {
    this.getAllSubCategory()
  }

  gotoCart() {
    this.route.navigate(['./product-checkout']);
  }


  public addProduct(index) {
    this.productList[index].quantity = this.productList[index].quantity + 1;
    // this.showCount = !this.showCount;
  }

  public addQuantity(index) {
    this.productList[index].quantity = this.productList[index].quantity + 1;
  }

  public removeQuantity(index) {
    if (this.productList[index].quantity != 0) {
      this.productList[index].quantity = this.productList[index].quantity - 1;
    }
  }

  gotoProductDetails(item) {
    // this.route.navigate(['./product-details']);
    console.log(this.productList)
    let navigationExtras: NavigationExtras = {
      queryParams: {
        product_id: item.id,
        others_products: JSON.stringify(this.productList)
      }
    };
    this.route.navigate(['./product-details'], navigationExtras);
  }


  getCartCount() {
    this.apiService.homePage('cart/count')
      .then((success: any) => {
        if (success.status == 'success') {
          console.log(success)
          let masterData = success.data;
          this.cartCount = masterData?.cartcount;
        } else {
          this.global.presentToast(success.message);
        }

      }).catch((err: any) => {
        console.log(err);
        this.global.presentToast(err.message);
      })
  }

  getAllSubCategory() {
    let data = {
      parent_id: this.global.selectedCategory
    }
    this.apiService.apiWithoutToken(data, 'categories/fetch')
      .then((success: any) => {
        if (success.status == 'success') {
          // this.global.presentLoadingClose()
          console.log(success)
          let masterData = success.data;
          this.subcategoryList = masterData.categories;


          // var getsegment = document.querySelector('ion-segment');
          //  getsegment.value = this.segment;
          //  console.log(getsegment.value)
          // console.log(getsegment)
          // var active = getsegment.querySelectorAll('ion-segment-button')[this.segment];
          // console.log(active)
          // if (active) {
          // this.segment.scrollIntoView({ behavior: "smooth", inline: "center" })
          // }



          this.loading = true;
          this.changeSubcat(null)
          // this.segment = this.subcategoryList[0].id;
          setTimeout(() => {
            this.loading = false
          }, 1000)
        } else {
          //this.global.presentLoadingClose()
          this.global.presentToast(success.message);
          setTimeout(() => {
            this.loading = false
          }, 1000)
        }

      }).catch((err: any) => {
        console.log(err);
        // this.global.presentLoadingClose()
        this.global.presentToast(err.message);
        setTimeout(() => {
          this.loading = false
        }, 1000)
      })
  }

  doRefresh(event) {
    this.changeSubcat(event)
  }
  changeSubcat(event) {
    console.log(event)
    let data = {
      category_id: this.segment
    }

    //this.slider.slideTo(4);
    //  var segment = document.querySelector('ion-segment');
    //segment.value = this.segment;
    //console.log(segment)
    //let el = document.getElementById('4');
    //el.scrollIntoView({behavior: 'smooth', block: 'start', inline: 'nearest'});
    // var active = segment.querySelectorAll('ion-segment-button')[this.segment];
    // if (active) {
    //   console.log(segment.querySelectorAll('ion-segment-button'))
    //   active.scrollIntoView({ behavior: "smooth", inline: "center" })
    // }

    // document.getElementById("15").scrollIntoView({
    //   behavior: 'smooth',
    //   block: 'center',
    //   inline: 'center'
    // });


    if (event == 'loading') {
      this.loading = true
    }
    // this.apiService.apiWithoutToken(data,'products/browse')
    this.apiService.findProductList(data)
      .then((success: any) => {
        if (success.status == 'success') {
          console.log(success)
          let masterData = success.data;
          this.productList = masterData.products;
          setTimeout(() => {
            this.loading = false;
            const el = document.getElementById(this.segment);
            el.scrollIntoView({ behavior: "smooth",inline: "center" });
          }, 1000)
          if (event != null && event != 'loading') { event.target.complete(); }
        } else {
          this.global.presentToast(success.message);
          setTimeout(() => {
            this.loading = false;
            const el = document.getElementById(this.segment);
            el.scrollIntoView({ behavior: "smooth",inline: "center" });
          }, 1000)
          if (event != null && event != 'loading') { event.target.complete(); }
        }
      }).catch((err: any) => {
        console.log(err);
        setTimeout(() => {
          this.loading = false;
          const el = document.getElementById(this.segment);
          el.scrollIntoView({ behavior: "smooth",inline: "center" });
        }, 1000)
        this.global.presentToast(err.message);
        if (event != null && event != 'loading') { event.target.complete(); }
      })
  }


  /* == LOAD subcategory wise product list ============ */
  public loadSubCat_wiseProductList() {
    let data = {
      category_id: this.segment
    }
    // this.apiService.apiWithoutToken(data,'products/browse')
    this.apiService.findProductList(data)
      .then((success: any) => {
        this.global.presentLoadingClose()
        if (success.status == 'success') {
          console.log(success)
          let masterData = success.data;
          this.productList = masterData.products;
        } else {
          this.global.presentToast(success.message);
        }
      }).catch((err: any) => {
        this.global.presentLoadingClose();
        console.log(err);
        this.global.presentToast(err.message);
      })
  }

  public addToCart(item, value) {
    // this.showIncDescBtn = !this.showIncDescBtn
    this.global.presentLoadingDefault();
    let paramData = {
      variant_id: item?.product_variants[0]?.id,
      quantity: value
    }
    this.apiService.addToCart(paramData)
      .then((success: any) => {
        console.log("Cart msg : ", success)
        // this.global.presentLoadingClose();
        if (success.status == 'success') {
          this.global.presentToast(success.message);
          if (success.data) {
            this.global.globalCartCount = success.data.cartcount;
            this.global.globalCartPrice = success.data.carttotal;
          }
          // this.loadHomepageData();
          this.loadSubCat_wiseProductList();
        } else {
          this.global.presentLoadingClose()
          this.global.presentToast(success.message);
        }

      })
      .catch((error: any) => {
        this.global.presentLoadingClose()
        console.log("Login error : ", error)
      })
  }

}
