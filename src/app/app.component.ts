import { Component, OnInit, Inject, ViewChildren, QueryList, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, MenuController, IonRouterOutlet } from '@ionic/angular';
import { ApiService } from './services/api.service';
import { GlobalService } from './services/global.service';

import { Platform, NavController } from '@ionic/angular';

//Import the Social Sharing plugin 
// import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})

export class AppComponent {
  backButtonSubscription;
  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;
  
  lastTimeBackPress = Date.now();
  timePeriodToExit = 2000;
  constructor(
    public route: Router,
    private menu: MenuController,
    public global: GlobalService,
    private apiService: ApiService,
    private platform: Platform,
    public alertController: AlertController,
    public navCtrl: NavController,
    private socialSharing: SocialSharing,

  ) {
    this.initializeApp()
  }


  initializeApp() {
    this.platform.ready().then(() => {
      this.global.setPushNotification();
     // this.backButtonEvent();
      this.checkLogin()
    });
  }

  checkLogin() {
    let userAccessToken = localStorage.getItem('access_token');
    if (userAccessToken) {
      this.global.user_token = userAccessToken;
      let local_userDetails = localStorage.getItem("user_details");
      this.global.userdetails = JSON.parse(local_userDetails);
      console.log("user details : ", this.global.userdetails);
      // this.route.navigate(['./main-dashboard']);
      this.navCtrl.navigateRoot(['./main-dashboard']);
    } else {
      let getStartedFlag = localStorage.getItem('getStartedFlag');
      if (getStartedFlag) {
        this.route.navigate(['./login-screen']);
        // this.route.navigate(['./otp-screen']);
      } else {
        this.route.navigate(['./intro']);
      }

    }
  }

  viewProfile() {
    this.menu.close();
    this.route.navigate(['./profile']);
  }

  navigateURL(url) {
    if (url != '') {
      this.menu.close();
      this.route.navigate([url]);
    } else {
      this.menu.close();
      const url = "https://play.google.com/store/apps/details?id=com.techinvein.arambaghplaza"
      const text = "*Arambagh Plaza* is a low-price online supermarket that allows you to order products across categories like Grocery, Beauty & Wellness, Household Items, Baby care, etc and gets them delivered to your doorstep. *Download it and try it!*" + '\n'
      this.socialSharing.share(text, 'MEDIUM', null, url)

    }
  }

  support() {
    this.menu.close();
    this.route.navigateByUrl('/support');

  }

  logout() {
    this.alertController.create({
      header: 'Logout',
      message: 'Do you want to logout?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Let me think');
          }
        },
        {
          text: 'Yes!',
          handler: () => {
            localStorage.removeItem("access_token")
            localStorage.removeItem("user_details")
            this.global.user_token = "";
            this.global.userdetails = {};
            this.menu.close();
            this.navCtrl.navigateRoot(['./login-screen']);
          }
        }
      ]
    }).then(res => {
      res.present();
    });
  }
  changePassword() {
    this.menu.close();
    this.route.navigate(['./change-password']);
  }

  backButtonEvent() {
    document.addEventListener("backbutton", () => {
      this.routerOutlets.forEach((outlet: IonRouterOutlet) => {
        if (outlet && outlet.canGoBack()) {
          outlet.pop();
        } else if (this.route.url === './main-dashboard') {
          if (Date.now() - this.lastTimeBackPress < this.timePeriodToExit) {
            navigator['app'].exitApp();
          } else {
            this.global.presentAlert('Press back again to exit App');
            this.lastTimeBackPress = new Date().getTime();
          }
        }
      })
    });
  }
}
