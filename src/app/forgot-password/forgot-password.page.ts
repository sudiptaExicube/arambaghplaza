import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalService } from '../services/global.service';
import { MenuController, NavController } from '@ionic/angular';
import { ApiService } from '../services/api.service';
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {
  public email:any = "";
  constructor(
    public global: GlobalService,
    private route: Router,
    private menu: MenuController,
    private apiService: ApiService,
    public navCtrl:NavController
  ) { }

  ngOnInit() {
  }

  forgotPassword(){
    if (!this.email.trim()) {
      this.global.presentToast("Mobile number field cannot be blank")
    } else if (this.email.length <10) {
      this.global.presentToast("Mobile number should be 10 digit")
    } else {
      this.global.presentLoadingDefault();
      let details = {
        "mobile": this.email,
        
      }
      this.apiService.apiWithoutToken(details,'auth/password/reset').then((res: any) => {
        console.log(res);
       if(res.status == "success"){
         this.global.presentToast(res.message)
        this.global.presentLoadingClose();
        this.navCtrl.pop()
       }else{
        this.global.presentToast(res.message)
        this.global.presentLoadingClose()
       }
      }).catch((err: any) => {
        console.log(err);
        this.global.presentToast('Sorry!! Something went to wrong.');
        this.global.presentLoadingClose()
      })
    }
  }

}
