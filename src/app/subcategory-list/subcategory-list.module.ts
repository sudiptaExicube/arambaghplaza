import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SubcategoryListPageRoutingModule } from './subcategory-list-routing.module';

import { SubcategoryListPage } from './subcategory-list.page';
import { LazyLoadImageModule } from 'ng-lazyload-image';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SubcategoryListPageRoutingModule,
    LazyLoadImageModule
  ],
  declarations: [SubcategoryListPage]
})
export class SubcategoryListPageModule {}
