import { Component, OnInit, NgZone, ViewChild, ElementRef } from '@angular/core';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { GlobalService } from '../services/global.service';
import { ModalController } from '@ionic/angular';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx'
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { Platform } from '@ionic/angular';
declare var google: any;
@Component({
  selector: 'app-address-modal',
  templateUrl: './address-modal.page.html',
  styleUrls: ['./address-modal.page.scss'],
})
export class AddressModalPage implements OnInit {
  @ViewChild('map', { static: false }) mapElement: ElementRef;
  map: any;
  address: string;
  lat: string;
  long: string;
  autocomplete: { input: string; };
  autocompleteItems: any[];
  location: any;
  placeid: any;
  GoogleAutocomplete: any;
  locationPermission: boolean;
  constructor(private nativeGeocoder: NativeGeocoder,
     public platform: Platform,
     private locationAccuracy: LocationAccuracy, 
     private androidPermissions: AndroidPermissions, 
     public modalController: ModalController, 
     public global: GlobalService, 
     private geolocation: Geolocation, 
     public zone: NgZone,) {
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
    this.autocomplete = { input: '' };
    this.autocompleteItems = [];
  }

  ngOnInit() { }

  permission() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION).then(
      result => {
        console.log('Has permission?', result.hasPermission);
        if (result.hasPermission == true) {
          this.locationPermission = true;
          this.locationAccuracy.canRequest().then((canRequest: boolean) => {
            if (canRequest) {
              // the accuracy option will be ignored by iOS
              this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
                () => {
                  console.log('Request successful')
                  this.getAddress()
                },
                error => console.log('Error requesting location permissions', error)
              );
            }

          });

        } else {
          this.locationPermission = false;
          this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION).then(
            result => {
              if (result.hasPermission == true) {
                this.locationPermission = true;
                this.locationAccuracy.canRequest().then((canRequest: boolean) => {
                  if (canRequest) {
                    // the accuracy option will be ignored by iOS
                    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
                      () => {
                        console.log('Request successful')
                        this.getAddress();
                      },
                      error => console.log('Error requesting location permissions', error)
                    );
                  }

                });
              }
            })
        }
      },
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION)
    );
  }

  getAddress() {
    this.geolocation.getCurrentPosition().then((resp) => {
      console.log(resp)
      let data = {
        lat: resp.coords.latitude,
        lng: resp.coords.longitude
      }
      this.global.getAddressobj(data.lat, data.lng).then((res: any) => {
        console.log('calculation area from current adddrerss===>', res);
        if (res) {
          console.log(res)
          let addr = res.results[0].address_components;
          let address = res.results[0].formatted_address;
          if (addr) {
            console.log(addr)
            for (let i = 0; i < addr.length; i++) {
              let addrtype = addr[i].types
              for (let j = 0; j < addrtype.length; j++) {
                if (addrtype[j] == 'postal_code') {
                  var pincode = addr[i].long_name;
                } if (addrtype[j] == 'administrative_area_level_1') {
                  var state = addr[i].long_name;;
                } if (addrtype[j] == 'locality') {
                  var city = addr[i].long_name;;
                }
              }
            }
            console.log(pincode);
            console.log(state)
            console.log(city)
            this.modalController.dismiss({
              latitude: data.lat,
              longitude: data.lng,
              address: address,
              zip_code: pincode ? pincode : "",
              city: city ? city : "",
              state: state ? state : ""
            });

            console.log('calculation area end  ===>')
          }
        }
      })
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  closeModal() {
    this.modalController.dismiss(null)
  }
  //AUTOCOMPLETE, SIMPLY LOAD THE PLACE USING GOOGLE PREDICTIONS AND RETURNING THE ARRAY.
  UpdateSearchResults() {
    if (this.autocomplete.input == '') {
      this.autocompleteItems = [];
      return;
    }
    this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete.input },
      (predictions, status) => {
        this.autocompleteItems = [];
        this.zone.run(() => {
          predictions.forEach((prediction) => {
            this.autocompleteItems.push(prediction);
          });
        });
      });
  }

  //wE CALL THIS FROM EACH ITEM.
  SelectSearchResult(item, from) {
    if (from == 'curAddr') {
      console.log(item)
      console.log('from current address')
      this.permission();
    } else {

      console.log('from addr list', item)
      this.placeid = item.place_id
      this.global.geoCode(item.description).then((success: any) => {
        console.log('res', success);
        this.autocompleteItems = [];
        let address = item.description
        let latitude = success.geometry.location.lat();
        let longitude = success.geometry.location.lng();

        console.log(latitude)
        console.log(address)
        console.log(longitude)

        console.log('calculation area ===>')
        let addr = success.address_components;
        if (addr) {
          console.log(addr)
          for (let i = 0; i < addr.length; i++) {
            let addrtype = addr[i].types
            for (let j = 0; j < addrtype.length; j++) {

              if (addrtype[j] == 'postal_code') {
                var pincode = addr[i].long_name;
              } if (addrtype[j] == 'administrative_area_level_1') {
                var state = addr[i].long_name;;
              } if (addrtype[j] == 'locality') {
                var city = addr[i].long_name;;
              }
            }
          }
          console.log(pincode);
          console.log(state)
          console.log(city)
          this.modalController.dismiss({
            latitude: latitude,
            longitude: longitude,
            address: address,
            zip_code: pincode ? pincode : "",
            city: city ? city : "",
            state: state ? state : ""
          });

          console.log('calculation area end  ===>')
        }
      })
    }

  }


  //lET'S BE CLEAN! THIS WILL JUST CLEAN THE LIST WHEN WE CLOSE THE SEARCH BAR.
  ClearAutocomplete() {
    this.autocompleteItems = []
    this.autocomplete.input = ''
  }
}
