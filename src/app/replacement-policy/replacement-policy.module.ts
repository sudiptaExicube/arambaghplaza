import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReplacementPolicyPageRoutingModule } from './replacement-policy-routing.module';

import { ReplacementPolicyPage } from './replacement-policy.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReplacementPolicyPageRoutingModule
  ],
  declarations: [ReplacementPolicyPage]
})
export class ReplacementPolicyPageModule {}
