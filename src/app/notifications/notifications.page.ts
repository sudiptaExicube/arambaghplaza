import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {
  // loading:boolean=false;
  loading: boolean = false;
  nofiArr: any = [];
  constructor(
    public apiservice: ApiService,
    public global: GlobalService,

  ) {
    setTimeout(() => {
      this.loading = false
    }, 1000)
  }

  ionViewWillEnter() {
    this.loading = true;
    this.findNotificationList(null)
  }

  ngOnInit() {
  }

  findNotificationList(event) {
    this.apiservice.fetchNotifications()
      .then((success: any) => {
        setTimeout(() => {
          this.loading = false
        }, 1000)
        console.log(success)
        this.nofiArr = success?.data?.notifications
        if (event != null) { event.target.complete(); }
      })
      .catch((error: any) => {
        setTimeout(() => {
          this.loading = false
        }, 1000)
        if (event != null) { event.target.complete(); }
      })
  }

  doRefresh(event) {
    this.findNotificationList(event)
  }
}
