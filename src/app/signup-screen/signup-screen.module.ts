import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SignupScreenPageRoutingModule } from './signup-screen-routing.module';

import { SignupScreenPage } from './signup-screen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SignupScreenPageRoutingModule
  ],
  declarations: [SignupScreenPage]
})
export class SignupScreenPageModule {}
