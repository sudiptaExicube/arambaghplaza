import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-product-search',
  templateUrl: './product-search.page.html',
  styleUrls: ['./product-search.page.scss'],
})
export class ProductSearchPage implements OnInit {
  showLineLoader:boolean = false
  public searchValue:any="";
  public productList:any=[];
  public defaultImage:any = 'https://miro.medium.com/max/441/1*9EBHIOzhE1XfMYoKz1JcsQ.gif';
  constructor(
    private route: Router,
    public global: GlobalService,
    private menu: MenuController,
    private apiService: ApiService
  ) { }

  ngOnInit() {
  }

  gotoCart(){
    this.route.navigate(['./product-checkout']);
  }
  gotoProductDetails(item){
    // this.route.navigate(['./product-details']);
    let navigationExtras: NavigationExtras = {
      queryParams: {
        product_id: item.id,
        others_products:JSON.stringify(this.productList)
      }
    };
    this.route.navigate(['./product-details'], navigationExtras);
  }

  public checkSearchValue(searchdata){
    // this.searchValue = searchdata;
    this.searchValue = searchdata?.target?.value
    this.findSearchProduct(true)
  }

  public findSearchProduct(searchLoading){
    if (this.searchValue.length >= 3) {
      if(searchLoading != null){
        this.showLineLoader = true;
      }
      let params={
        searchkey:this.searchValue
      }

      this.apiService.searchProduct(params)
      .then((success:any)=>{
        if(searchLoading != null){
          this.showLineLoader = false;
        }
        if(success.status == 'success'){
          console.log(success)
          this.productList=[]
          let masterData = success.data;
          this.productList = masterData.products;
        }else{
          this.global.presentToast(success.message);
        }
      }).catch((err: any) => {
        if(searchLoading != null){
          this.showLineLoader = false;
        }
        this.global.presentToast(err.message);
      })


    }else{
      this.productList=[]
    }
  }

  public addToCart(item,value){
    // this.showIncDescBtn = !this.showIncDescBtn
    this.global.presentLoadingDefault();
    let paramData={
      variant_id: item?.product_variants[0]?.id,
      quantity:value
    }
    this.apiService.addToCart(paramData)
      .then((success:any)=>{
        console.log("Cart msg : ", success)
        this.global.presentLoadingClose();
        if(success.status == 'success'){
          this.global.presentToast(success.message);
          if(success.data){
            this.global.globalCartCount = success.data.cartcount;
            this.global.globalCartPrice = success.data.carttotal;
          }
          this.findSearchProduct(null)
        }else{
          this.global.presentLoadingClose()
          this.global.presentToast(success.message);
        }
        
      })
      .catch((error:any)=>{
        this.global.presentLoadingClose()
        console.log("Login error : ", error)
      })


  }

}
