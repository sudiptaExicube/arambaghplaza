import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform, NavController, AlertController, PopoverController } from '@ionic/angular';
import { GlobalService } from '../services/global.service';
import { MenuController } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { KnowMorePage } from '../know-more/know-more.page';
// import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  width: any;
  public userDetails: any = {
    email: "",
    profile_picture_path: "",
    phone_no: "",
    name: "",
  };

  tempPhoto: any = ""
  constructor(
    platform: Platform,
    private route: Router,
    public global: GlobalService,
    private menu: MenuController,
    private apiService: ApiService,
    public navCtrl: NavController,
    public alertController: AlertController,
    public popoverController: PopoverController,
    private socialSharing: SocialSharing,
  ) {
    platform.ready().then(() => {
      console.log('Width: ' + platform.width());
      console.log('Height: ' + platform.height());
      this.width = platform.width() / 2.9 + 'px'
      console.log('Width: ' + this.width);
    });

    this.userDetails = this.global.userdetails;
    console.log(this.userDetails)
  }

  ngOnInit() {

  }

  ionViewWillEnter() {
    this.getProfile();
  }

  getProfile() {
    this.global.presentLoadingDefault();
    this.apiService.getApi('auth/account')
      .then((success: any) => {
        console.log(success)
        if (success.status == 'success') {
          let successdata: any = success.data;
          localStorage.setItem("user_details", JSON.stringify(successdata.user))
          this.global.userdetails = successdata.user;
          this.global.selectedScheme = successdata?.scheme
          //this.global.presentToast(success.message);
          this.global.presentLoadingClose()
        } else {
          this.global.presentToast(success.message);
          this.global.presentLoadingClose()
        }

      }).catch((err: any) => {
        console.log(err);
        this.global.presentToast(err.message);
        this.global.presentLoadingClose()
      })
  }
  editAccount() {
    this.route.navigate(['./edit-account']);
  }

 

  async viewComission() {
    const popover = await this.popoverController.create({
      component: KnowMorePage,
      cssClass: 'contact-popover',
      translucent: true,
      componentProps: {data:this.global.selectedScheme}
    });
    await popover.present();
  
    const { role } = await popover.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  
  }

  changePassword() {
    this.route.navigate(['./change-password']);
  }

  manageAddr() {
    this.route.navigate(['./address-list']);
  }

  myOrders() {
    this.route.navigate(['./my-orders']);
  }

  wallet(){
    this.route.navigate(['./wallet']);
  }

  about() {
    this.route.navigate(['./about-us']);
  }

  support() {
    this.route.navigate(['./support-list']);
  }

  wishlist(){
    this.route.navigate(['./wishlist']);
  }

  shareApp(){
    const url = "https://play.google.com/store/apps/details?id=com.techinvein.arambaghplaza"
      const text = "*Arambagh Plaza* is a low-price online supermarket that allows you to order products across categories like Grocery, Beauty & Wellness, Household Items, Baby care, etc and gets them delivered to your doorstep. *Download it and try it!*" + '\n'
      this.socialSharing.share(text, 'MEDIUM', null, url)
  }

  joinProgram(){
    this.route.navigate(['./earning-program']);
  }

  logout() {
    this.alertController.create({
      header: 'Logout',
      message: 'Do you want to logout?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Let me think');
          }
        },
        {
          text: 'Yes!',
          handler: () => {
            localStorage.removeItem("access_token")
            localStorage.removeItem("user_details")
            this.global.user_token = "";
            this.global.userdetails = {};
            this.global.selectedScheme = {};
            this.menu.close();

            this.route.navigate(['./login-screen']);
          }
        }
      ]
    }).then(res => {
      res.present();
    });

  }




}

