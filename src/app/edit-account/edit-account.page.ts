import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, Platform } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-edit-account',
  templateUrl: './edit-account.page.html',
  styleUrls: ['./edit-account.page.scss'],
})
export class EditAccountPage implements OnInit {

  public first_name: any = "";
  public email: any = "";
  public mobile: any = ""

  constructor(
    public global: GlobalService,
    private route: Router,
    private menu: MenuController,
    private apiService: ApiService,
    private platform: Platform,
  ) { }

  ngOnInit() {
    if (this.global.userdetails) {
      this.first_name = this.global.userdetails?.name;
      this.email = this.global.userdetails?.email;
      this.mobile = this.global.userdetails?.mobile;
    }
  }

  isvalidEmailFormat(email) {
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}

  charecterValidation(text) {
    var charRegex = /^([a-zA-Z]+\s)*[a-zA-Z]+$/
    return charRegex.test(text);
  }

  goLogin() {
    if (!this.first_name) {
      this.global.presentToast("Full name field cannot be blank");
    } else if (!this.charecterValidation(this.first_name)) {
      this.global.presentToast("Please enter A-Z characters for full name");
    } else if (!this.email) {
      this.global.presentToast("Email field cannot be blank")
    }else if (!this.isvalidEmailFormat(this.email)) {
      this.global.presentToast("Please enter valid email");
		} else {
      this.global.presentLoadingDefault();
      let details = {
        "name": this.first_name,
        "email": this.email,

      }
      this.apiService.apiWithTokenBody(details, 'user/update/basic-details').then((success: any) => {
        console.log(success);
        if (success.status == 'success') {
          this.global.presentToast(success.message);
          this.global.presentLoadingClose()
        } else {
          this.global.presentToast(success.message);
          this.global.presentLoadingClose()
        }

      }).catch((err: any) => {
        console.log(err);
        this.global.presentToast(err.message);
        this.global.presentLoadingClose()
      })
    }
  }

}
