import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { GlobalService } from './../services/global.service';
import { ApiService } from './../services/api.service';

@Component({
  selector: 'app-support',
  templateUrl: './support.page.html',
  styleUrls: ['./support.page.scss'],
})
export class SupportPage implements OnInit {
  order_code: any = "";
  typeArr: any = [];
  subject: any = ""
  name: any;
  email: any;
  mobile: any;
  message: any;
  tick: boolean = false
  constructor(private route: Router,
    public global: GlobalService,
    private apiService: ApiService,
    public activeRoute: ActivatedRoute,) {
    this.activeRoute.queryParams.subscribe(params => {
      console.log("params : ", params)
      if (params) {
        this.order_code = params.order_id;
        this.name = this.global.userdetails?.name
        this.email = this.global.userdetails?.email
        this.mobile = this.global.userdetails?.mobile
      }

    })

  }

  ngOnInit() {
    this.getSupportType()
  }

  checkValue() {

  }

  getSupportType() {
    this.global.presentLoadingDefault();
    this.apiService.apiWithToken('support-ticket/fetch-subjects')
      .then((success: any) => {
        if (success.status == 'success') {
          console.log(success)
          let masterData = success.data;
          let data = masterData?.subjects;
          console.log(this.order_code)
          if (this.order_code == undefined) {
            console.log('iii')
            for (let i = 0; i < data.length; i++) {
              if (data[i].type == 'contactrequest') {
                this.typeArr.push(data[i])
              }
            }
          } else {
            this.typeArr = masterData?.subjects;
          }
          this.global.presentLoadingClose()
        } else {
          this.global.presentToast(success.message);
          this.global.presentLoadingClose()
        }

      }).catch((err: any) => {
        console.log(err);
        this.global.presentToast(err.message);

      })
  }

  isvalidEmailFormat(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  read() {
    this.route.navigate(['./replacement-policy']);
  }

  supportReq() {
    if (!this.name) {
      this.global.presentToast("Please enter name");
    } else if (!this.mobile) {
      this.global.presentToast("Please enter mobile number");
    } else if (!this.email) {
      this.global.presentToast("Please enter email");
    } else if (!this.isvalidEmailFormat(this.email)) {
      this.global.presentToast("Please enter valid email");
    } else if (!this.subject) {
      this.global.presentToast("Please select subject");
    } else if (!this.message) {
      this.global.presentToast("Please share reason about subject");
    } else if (this.subject != "contactrequest" && this.tick == false) {
      this.global.presentToast("Please mark on tick for accept our policy");
    }
    else {
      this.global.presentLoadingDefault();
      console.log(this.order_code)
      this.apiService.apiWithTokenBody({ name: this.name, mobile: this.mobile, email: this.email, subject: this.subject, order_code: this.order_code ? this.order_code : "", message: this.message }, 'support-ticket/submit')
        .then((success: any) => {
          console.log(success)
          this.global.presentLoadingClose()
          if (success.status == 'success') {
            this.global.presentToast(success.message);
            let masterData = success.data;
            console.log(masterData);
            //this.global?.store_details?.whatsapp
            if (this.order_code == undefined) {
              let data = "*Hi Team Arambagh Plaza,*" + "%0a" + "%0a" + "Ticket No - " + masterData?.ticket?.code + "%0a" + "---------------------------------------------------------" + "%0a" + "Support For - " + this.subject + "%0a" + "---------------------------------------------------------" + "%0a" + "Message - " + this.message + "%0a" + "---------------------------------------------------------" + "%0a" +  "Name - " + this.name + "%0a" + "Mobile Number - " + this.mobile;
              console.log(data)
              window.open("https://api.whatsapp.com/send?phone=+917365833384&text=" + data, '_blank');
            }else{
              let data = "*Hi Team Arambagh Plaza,*" + "%0a" + "%0a" + "Ticket No - " + masterData?.ticket?.code + "%0a" + "---------------------------------------------------------" + "%0a" + "Support For - " + this.subject + "%0a" + "---------------------------------------------------------" + "%0a" + "Message - " + this.message + "%0a" + "---------------------------------------------------------" + "%0a" + "Order ID - " + this.order_code + "%0a" + "---------------------------------------------------------" + "%0a" + "Name - " + this.name + "%0a" + "Mobile Number - " + this.mobile;
              console.log(data)
              window.open("https://api.whatsapp.com/send?phone=+917365833384&text=" + data, '_blank');
            }
          } else {
            this.global.presentToast(success.message);
          }
        }).catch((err: any) => {
          this.global.presentLoadingClose()
          console.log(err);
          this.global.presentToast(err.message);
        })
    }
  }

}
