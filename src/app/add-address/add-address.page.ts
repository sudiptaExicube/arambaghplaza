import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { ModalController } from '@ionic/angular';
import { AddressModalPage } from './../address-modal/address-modal.page';
import { GlobalService } from '../services/global.service';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx'
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
declare var google;

import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../services/api.service';


@Component({
  selector: 'app-add-address',
  templateUrl: './add-address.page.html',
  styleUrls: ['./add-address.page.scss'],
})
export class AddAddressPage implements OnInit {

  @ViewChild('map', { static: false }) mapElement: ElementRef;
  map: any;

  locationPermission: boolean;

  name: any;
  mobile: any;
  location: any;
  latitude: any;
  longitude: any;
  address_line1: any;
  address_line2: any;
  postal_code: any;
  landmark: any;
  type: any = "home";
  id: any = null
  edit: boolean = false;
  constructor(
    public navCtrl: NavController,
    private route: Router,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    private locationAccuracy: LocationAccuracy,
    private androidPermissions: AndroidPermissions,
    private ngZone: NgZone,
    public global: GlobalService,
    public modalController: ModalController,
    public activeRoute: ActivatedRoute,
    private apiService: ApiService,
    public platform : Platform
  ) {
    //this.global.presentLoadingDefault()

  }

  ionViewWillEnter() {
    this.name = this.global.userdetails?.name;
    this.mobile = this.global.userdetails?.mobile;
    this.activeRoute.queryParams.subscribe(params => {
     // console.log("params : ", JSON.parse(params["details"]))
      // console.log(JSON.parse(params["full_address"]))
      if(params){
        if(params["details"]){
          let paramsData = JSON.parse(params["details"]);
          if(paramsData){
            this.location = paramsData["location"];
            this.latitude = paramsData["latitude"];
            this.longitude = paramsData["longitude"];
            this.postal_code = paramsData["full_address"]["postal_code"];
            this.name = paramsData["name"];
            this.mobile = paramsData["mobile"];
            this.address_line1 = paramsData["full_address"]["address_line1"];
            this.address_line2 = paramsData["full_address"]["address_line2"];
            this.landmark = paramsData["full_address"]["landmark"];
            this.type = paramsData["type"];
            this.id = paramsData["id"];

            this.loadMap('autocomplete')
          }
        }else{
          console.log("params not found")
          if(this.platform.is('cordova')){
            console.log("cordova found")
            this.permission()
          }else{
            console.log("cordova not found")
            this.loadMap('fromCurLocation');
          }
        }
      }else{
        console.log("params not found")
        if(this.platform.is('cordova')){
          console.log("cordova found")
          this.permission()
        }else{
          console.log("cordova not found")
          this.loadMap('fromCurLocation');
        }
      }
    })
    

  }


  ngOnInit() {
    // if(this.platform.is('cordova')){
    //   this.permission()
    // }else{
    //   this.loadMap('fromCurLocation');
    // }
    // this.activeRoute.queryParams.subscribe(params => {
    //   // console.log("params : ", JSON.parse(params["details"]))
    //    // console.log(JSON.parse(params["full_address"]))
    //    if(params){
    //      if(params["details"]){
    //        let paramsData = JSON.parse(params["details"]);
    //        if(paramsData){
    //          this.location = paramsData["location"];
    //          this.latitude = paramsData["latitude"];
    //          this.longitude = paramsData["longitude"];
    //          this.postal_code = paramsData["full_address"]["postal_code"];
    //          this.name = paramsData["name"];
    //          this.mobile = paramsData["mobile"];
    //          this.address_line1 = paramsData["full_address"]["address_line1"];
    //          this.address_line2 = paramsData["full_address"]["address_line2"];
    //          this.landmark = paramsData["full_address"]["landmark"];
    //          this.type = paramsData["type"];
    //          this.id = paramsData["id"];
 
    //          this.loadMap('autocomplete')
    //        }
    //      }
    //    }else{
    //      console.log("params not found")
    //      if(this.platform.is('cordova')){
    //        console.log("cordova found")
    //        this.permission()
    //      }else{
    //        console.log("cordova not found")
    //        this.loadMap('fromCurLocation');
    //      }
    //    }
    //  })
  }




  getDate() {
    let today = new Date();
    let todayDate = ('0' + (today.getDate())).slice(-2);
    let todayMonth = ('0' + (today.getMonth() + 1)).slice(-2);
    let lstyear = today.getFullYear();
    let maxdate = lstyear + '-' + todayMonth + '-' + todayDate;
    return (maxdate);
  }
  permission() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION).then(
      result => {
        console.log('Has permission?', result.hasPermission);
        if (result.hasPermission == true) {
          this.locationPermission = true;
          this.locationAccuracy.canRequest().then((canRequest: boolean) => {
            if (canRequest) {
              // the accuracy option will be ignored by iOS
              this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
                () => {
                  console.log('Request successful')
                  this.loadMap('fromCurLocation');
                },
                error => console.log('Error requesting location permissions', error)
              );
            }

          });

        } else {
          this.locationPermission = false;
          this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION).then(
            result => {
              if (result.hasPermission == true) {
                this.locationPermission = true;
                this.locationAccuracy.canRequest().then((canRequest: boolean) => {
                  if (canRequest) {
                    // the accuracy option will be ignored by iOS
                    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
                      () => {
                        console.log('Request successful')
                        this.loadMap('fromCurLocation');
                      },
                      error => console.log('Error requesting location permissions', error)
                    );
                  }

                });
              }
            })
        }
      },
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION)
    );
  }



  loadMap(from) {
    if (from == 'fromCurLocation') {
      this.geolocation.getCurrentPosition().then((resp) => {

        this.latitude = resp.coords.latitude;
        this.longitude = resp.coords.longitude;

        let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
        let mapOptions = {
          center: latLng,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          zoomControl: false,
          fullscreenControl: false,
          draggable: false
        }

        this.getAddressFromCoords(resp.coords.latitude, resp.coords.longitude);
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        this.getpin(this.latitude, this.longitude)
        this.map.addListener('dragend', () => {
          this.latitude = this.map.center.lat();
          this.longitude = this.map.center.lng();
          this.getAddressFromCoords(this.map.center.lat(), this.map.center.lng())

        });

      }).catch((error) => {
        console.log('Error getting location', error);
      });
    } else {


      let latLng = new google.maps.LatLng(this.latitude, this.longitude);
      let mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControl: false,
        fullscreenControl: false,
        draggable: false
      }

      this.getAddressFromCoords(this.latitude, this.longitude);
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      this.map.addListener('dragend', () => {
        this.latitude = this.map.center.lat();
        this.longitude = this.map.center.lng();
        this.getAddressFromCoords(this.map.center.lat(), this.map.center.lng())

      });
    }

  }

  getAddressFromCoords(lattitude, longitude) {
    console.log("getAddressFromCoords " + lattitude + " " + longitude);
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };

    this.nativeGeocoder.reverseGeocode(lattitude, longitude, options)
      .then((result: NativeGeocoderResult[]) => {
        this.location = "";
        let responseAddress = [];
        for (let [key, value] of Object.entries(result[0])) {
          if (value.length > 0)
            responseAddress.push(value);

        }
        responseAddress.reverse();
        for (let value of responseAddress) {
          this.location += value + ", ";
        }
        this.location = this.location.slice(0, -2);
      })
      .catch((error: any) => {
        this.location = "Address Not Available!";
      });

  }
  async addressModal() {
    const modal = await this.modalController.create({
      component: AddressModalPage,
      cssClass: 'my-custom-class'
    });
    await modal.present();
    modal.onDidDismiss()
      .then((data: any) => {
        console.log(data);
        if (data != null) {
          let addressData = data.data
          if (addressData != undefined) {
            console.log(addressData)
            this.latitude = addressData.latitude;
            this.longitude = addressData.longitude;
            this.location = addressData.address;
            this.postal_code = addressData.zip_code
            this.loadMap('autocomplete')
          }
        }
      });
  }


  getpin(lat, lng) {
    console.log('Fetch pincode....')
    this.global.getAddressobj(lat, lng).then((res: any) => {
      console.log('calculation area from current adddrerss===>', res);
      if (res) {
        console.log(res)
        let addr = res.results[0].address_components;
        let address = res.results[0].formatted_address;
        if (addr) {
          console.log(addr)
          for (let i = 0; i < addr.length; i++) {
            let addrtype = addr[i].types
            for (let j = 0; j < addrtype.length; j++) {
              if (addrtype[j] == 'postal_code') {
                this.postal_code = addr[i].long_name;
              }
            }
          }
          console.log(this.postal_code);
        }
      }
    })
  }

  isvalidphoneFormat(phone) {
		var re = /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[6789]\d{9}$/;
		return re.test(phone);
	}

  charecterValidation(text) {
    var charRegex = /^([a-zA-Z]+\s)*[a-zA-Z]+$/
    return charRegex.test(text);
  }

  addAddress() {
    if (!this.location) {
      this.global.presentToast("Please choose location");
    } else if (!this.postal_code) {
      this.global.presentToast("Please enter postal code");
    } else if (!this.name) {
      this.global.presentToast("Please enter billing name");
    }else if (!this.charecterValidation(this.name)) {
      this.global.presentToast("Please enter A-Z characters for billing name");
    } else if (!this.mobile) {
      this.global.presentToast("Please enter mobile number");
    }else if(this.mobile.length<10){
      this.global.presentToast("Mobile number should be 10 digit")
    } else if (!this.address_line1) {
      this.global.presentToast("Please enter address line 1");
    } else if (!this.address_line2) {
      this.global.presentToast("Please enter address line 2");
    } else if (!this.landmark) {
      this.global.presentToast("Please enter landmark");
    } else if (!this.type) {
      this.global.presentToast("Please select address type");
    } else {

      this.global.presentLoadingDefault();
      if (!this.edit) {
        let data = {
          location: this.location,
          latitude: this.latitude,
          longitude: this.longitude,
          postal_code: this.postal_code,
          name: this.name,
          mobile: this.mobile,
          address_line1: this.address_line1,
          address_line2: this.address_line2,
          landmark: this.landmark,
          type: this.type,
        }
        console.log(data);
        this.apiService.apiWithTokenBody(data,'user/address/add')
          .then((success: any) => {
            console.log(success)
            this.global.presentLoadingClose()
            if (success.status == 'success') {
              console.log(success)
              this.global.presentToast(success.message);
              this.navCtrl.pop()
            } else {
              this.global.presentToast(success.message);
            }
          }).catch((err: any) => {
            console.log(err);
            this.global.presentToast(err.message);
            this.global.presentLoadingClose();
          })
      } else {
        let data = {
          location: this.location,
          latitude: this.latitude,
          longitude: this.longitude,
          postal_code: this.postal_code,
          name: this.name,
          mobile: this.mobile,
          address_line1: this.address_line1,
          address_line2: this.address_line2,
          landmark: this.landmark,
          type: this.type,
          id: this.id
        }
        console.log(data);
        this.apiService.apiWithTokenBody(data,'user/address/edit')
          .then((success: any) => {
            console.log(success)
            this.global.presentLoadingClose()
            if (success.status == 'success') {
              console.log(success)
              this.global.presentToast(success.message);
              this.navCtrl.pop()
            } else {
              this.global.presentToast(success.message);
            }
          }).catch((err: any) => {
            console.log(err);
            this.global.presentToast(err.message);
            this.global.presentLoadingClose();
          })
      }
    }
  }




}

